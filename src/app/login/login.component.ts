import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private auth: AuthService, private route: ActivatedRoute,private router: Router) { }

  ngOnInit(): void {
  }

  invalidLogin = false;
  
  signIn(credentials){
    this.auth.login(credentials)
      .subscribe(response => {
        let returnUrl = this.route.snapshot.queryParamMap.get('returnUrl');
        this.router.navigate([returnUrl || '/']);
      },
      error => {
        this.invalidLogin = true;
      })
  }
}
