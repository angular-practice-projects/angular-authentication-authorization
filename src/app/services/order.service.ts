import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  private BASE_URL = "http://68.183.244.90/mastercheff/api/v1.0/";

  constructor(private http: HttpClient) { }

  getAll(){
    return {
      status:200,
      body: [1,2,3]
    };
  }
}
