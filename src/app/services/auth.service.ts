import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { JwtHelper } from 'angular-jwt';
import jwtDecode, * as jwt_decode from 'jwt-decode';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  
   private BASE_URL = "https://dhaval-assignment.herokuapp.com/";
  

  constructor(private http: HttpClient) { }

  login(credentials){
    return this.http.post(this.BASE_URL+'user/signin', credentials)
      .pipe(
        map(response => {
          let res = response as {user: Object, token: string};
          console.log(res);
          if(res && res.token)
          {
            localStorage.setItem('token',res.token);
            return true;
          }
          else
            return false;
      }));
  }

  logout(){
    localStorage.removeItem('token');
  }

  isLoggedIn(){
    let token = localStorage.getItem('token');
    console.log(token);
    if(token == null || token === "")
      return false;
    else
      return true;
  }
}



// private BASE_URL = "http://68.183.244.90/mastercheff/api/v1.0/user/login";